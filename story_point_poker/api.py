from datetime import timedelta
import asyncio
from starlette.responses import RedirectResponse, PlainTextResponse
from sse_starlette.sse import EventSourceResponse
from starlette.templating import Jinja2Templates


templates = Jinja2Templates(directory="templates")


async def new_session(request):
    storage = request.app.state.storage
    session_id = await storage.add_session()
    admin_id = await storage.add_participant(name="admin", is_admin=True, session_id=session_id)
    response = RedirectResponse(url=f"/sessions/{session_id}", status_code=303)
    response.set_cookie(
        "spp_user_id", admin_id, samesite="strict", max_age=int(timedelta(days=1).total_seconds())
    )
    return response


async def session_login(request):
    storage = request.app.state.storage
    session_id = request.path_params["session_id"]
    submitted = await request.form()
    user_name = submitted["user_name"]

    try:
        user_id = await storage.add_participant(name=user_name, session_id=session_id)
    except LookupError:
        return PlainTextResponse("session not found", status_code=404)
    except ValueError:
        return PlainTextResponse("user name already registered", status_code=409)

    response = RedirectResponse(url=f"/sessions/{session_id}", status_code=303)
    response.set_cookie(
        "spp_user_id", user_id, samesite="strict", max_age=int(timedelta(days=1).total_seconds())
    )
    return response


async def session_events(request):
    session_id = request.path_params["session_id"]
    user_id = request.cookies.get("spp_user_id")
    storage = request.app.state.storage

    async def event_generator():
        chan = storage.events_sub(session_id=session_id, user_id=user_id)
        try:
            while True:
                if await request.is_disconnected():
                    break
                _, event = await chan.get()
                yield {"event": event, "data": ""}
        except asyncio.CancelledError as err:
            raise err
        finally:
            remaining_connections = storage.events_unsub(
                session_id=session_id, user_id=user_id, chan=chan
            )
            if not remaining_connections:
                await storage.remove_participant(session_id=session_id, user_id=user_id)

    return EventSourceResponse(event_generator())


async def session_participants(request):
    session_id = request.path_params["session_id"]
    session = request.app.state.storage.get_session(session_id=session_id)
    participants = []
    for participant_id, participant in session["participants"].items():
        participants.append({**participant, "did_rate": participant_id in session["ratings"]})

    return templates.TemplateResponse(
        "session_participants.html",
        {"request": request, "participants": participants},
    )


async def set_rating(request):
    session_id = request.path_params["session_id"]
    user_id = request.cookies.get("spp_user_id")
    storage = request.app.state.storage
    submitted = await request.form()
    rating = submitted["rating"]
    try:
        await storage.set_rating(session_id=session_id, user_id=user_id, rating=rating)
    except LookupError:
        return PlainTextResponse("session or user not found", status_code=404)

    return PlainTextResponse(status_code=204)


async def get_ratings(request):
    session_id = request.path_params["session_id"]
    user_id = request.cookies.get("spp_user_id")
    storage = request.app.state.storage
    session = storage.get_session(session_id=session_id)
    user = storage.get_participant(session_id=session_id, user_id=user_id)
    options = [0, 1, 2, 3, 8, 13, 20, 40, 100]
    if session["state"] == "initial":
        return templates.TemplateResponse(
            "state_change.html",
            {
                "request": request,
                "user": user,
                "value": "rating",
                "label": "Start rating",
                "session_id": session_id,
            },
        )
    elif session["state"] == "rating":
        return templates.TemplateResponse(
            "rating_choices.html",
            {"request": request, "options": options, "session_id": session_id, "user": user},
        )
    elif session["state"] == "results":
        results = [
            {
                "user_id": user_id,
                "user_name": session["participants"][user_id]["name"],
                "value": int(rating_value),
            }
            for user_id, rating_value in session["ratings"].items()
        ]

        return templates.TemplateResponse(
            "rating_results.html",
            {"request": request, "user": user, "session_id": session_id, "results": results},
        )


async def set_state(request):
    session_id = request.path_params["session_id"]
    user_id = request.cookies.get("spp_user_id")
    storage = request.app.state.storage
    session = storage.get_session(session_id=session_id)
    if not session["participants"][user_id].get("is_admin", False):
        return PlainTextResponse("only admin can change session state", status_code=403)

    submitted = await request.form()
    state = submitted["state"]
    await storage.set_state(session_id=session_id, state=state)
    return PlainTextResponse(status_code=204)
