from contextlib import asynccontextmanager
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.routing import Route, Mount
from starlette.staticfiles import StaticFiles

from story_point_poker import pages, api
import aiochan as ac

import random


class Storage:
    def __init__(self):
        self._data = {"sessions": {}}
        self._events = ac.Chan()
        self.pub = self._events.pub()

    def close(self):
        self._events.close()

    def get_session(self, *, session_id):
        return self._data["sessions"][session_id]

    def get_participant(self, *, session_id, user_id):
        return self._data["sessions"][session_id]["participants"][user_id]

    async def add_participant(self, *, name, session_id, **attrs):
        if not session_id in self._data["sessions"]:
            raise LookupError("session with this id doesn't exist")

        participants = self._data["sessions"][session_id]["participants"]
        if any(participant["name"] == name for participant in participants.values()):
            raise ValueError("name is already registered")

        user_id = str(random.randint(1000, 9999))
        participants[user_id] = {
            "name": name,
            **attrs,
            "is_admin": not any(
                participant.get("is_admin") for participant in participants.values()
            ),
        }
        await self._events.put((session_id, "participants_updated"))
        return user_id

    async def remove_participant(self, *, session_id, user_id):
        try:
            del self._data["sessions"][session_id]["participants"][user_id]
        except LookupError:
            pass
        try:
            del self._data["sessions"][session_id]["ratings"][user_id]
        except LookupError:
            pass

        await self._events.put((session_id, "participants_updated"))

    async def add_session(self):
        sessions = self._data["sessions"]
        session_id = str(1 + max(map(int, sessions.keys()))) if sessions else "0"
        sessions[session_id] = {
            "participants": {},
            "state": "initial",
            "ratings": {},
            "connections": {},
        }
        return session_id

    async def set_rating(self, *, session_id, user_id, rating):
        self._data["sessions"][session_id]["ratings"][user_id] = rating
        await self._events.put((session_id, "participants_updated"))

    async def set_state(self, *, session_id, state):
        self._data["sessions"][session_id]["state"] = state
        await self._events.put((session_id, "session_state_updated"))
        if state == "rating":
            self._data["sessions"][session_id]["ratings"] = {}
            await self._events.put((session_id, "participants_updated"))

    def events_sub(self, *, session_id, user_id):
        chan = self.pub.sub(session_id)

        self._data["sessions"][session_id].setdefault("connections", {})
        self._data["sessions"][session_id]["connections"].setdefault(user_id, [])
        self._data["sessions"][session_id]["connections"][user_id].append(chan)
        return chan

    def events_unsub(self, *, session_id, user_id, chan):
        try:
            self.pub.unsub(session_id, chan)
            connections = self._data["sessions"][session_id]["connections"][user_id]
            connections.remove(chan)
            return connections
        except (LookupError, ValueError):
            return []


@asynccontextmanager
async def storage(app):
    app.state.storage = Storage()
    yield
    app.state.storage.close()


async def ping(request):
    return PlainTextResponse("pong")


routes = [
    Route("/ping", ping),
    Route("/", pages.root),
    Mount("/static", StaticFiles(directory="static")),
    Route("/sessions", api.new_session, methods=["POST"]),
    Route("/sessions/{session_id}", pages.session_root),
    Route("/sessions/{session_id}/login", api.session_login, methods=["POST"]),
    Route("/sessions/{session_id}/sse", api.session_events, methods=["GET"]),
    Route("/sessions/{session_id}/rating", api.set_rating, methods=["PUT"]),
    Route("/sessions/{session_id}/state", api.set_state, methods=["PUT"]),
    Route("/sessions/{session_id}/ratings", api.get_ratings, methods=["GET"]),
    Route("/sessions/{session_id}/participants", api.session_participants, methods=["GET"]),
]


app = Starlette(routes=routes, lifespan=storage)
