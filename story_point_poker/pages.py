from starlette.templating import Jinja2Templates
from starlette.responses import PlainTextResponse


templates = Jinja2Templates(directory="templates")


async def root(request):
    return templates.TemplateResponse("index.html", {"request": request, "enable_home_link": False})


async def session_root(request):
    storage = request.app.state.storage
    user_id = request.cookies.get("spp_user_id")
    session_id = request.path_params["session_id"]
    try:
        storage.get_session(session_id=session_id)
    except LookupError:
        return PlainTextResponse("session not found", status_code=404)

    try:
        user = storage.get_participant(session_id=session_id, user_id=user_id)
    except LookupError:
        return templates.TemplateResponse(
            "session_login.html", {"request": request, "session_id": session_id}
        )

    return templates.TemplateResponse(
        "session.html", {"request": request, "user": user, "session_id": session_id}
    )
