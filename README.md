# Story Point Poker

A simple app for [story point poker][planning poker].

## Why does it exist?

I wanted to play around with [sse][sse], and [htmx][htmx].
I also wanted to try some rudimentary frontend stuff with [jinja2][jinja2] templates served directly, and to try a bit *"lower"* level framework (like [starlette][starlette]) than the stuff I use at work.


[planning poker]: https://en.wikipedia.org/wiki/Planning_poker
[sse]: https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events
[starlette]: https://www.starlette.io/
[jinja2]: https://jinja2docs.readthedocs.io/en/stable/
[htmx]: https://htmx.org/
